#ifndef RestClient_h
#define RestClient_h

#include <Arduino.h>
#include <SPI.h>
#include "Client.h"

//̣ Uncoment this define to enable Serial prints of what's going on .
// #define DEBUG_REST

#ifdef DEBUG_REST
#define DEBUG_REST(args...) Serial.println(args);
#endif

#ifndef DEBUG_REST
#define DEBUG_REST(...)
#endif

class RestClient {

  public:
    RestClient(Client* _client, const char* host);
    RestClient(Client* _client, const char* _host, int _port);

    void setHeader(const char* header, const char* value);
    void setContentType(const char*);
    void setAgent(const char*);
    void setNetworkDelay(int);
    void setNetworkTimeout(int);
    void setHostPort(const char* _host, int _port);

    //Generic HTTP Request
    int request(const char* method, const char* path,
                const char* body, String* response);

    // GET path
    int get(const char*);
    // GET path and response
    int get(const char*, String*);
    // POST path and body
    int post(const char* path, const char* body);
    // POST path and body and response
    int post(const char* path, const char* body, String*);
    // PUT path and body and reponse
    int put(const char* path, const char* body, String*);

  private:
    Client* client;
    int readResponse(String*);
    void write(const char*);
    void writeln(const char*);
    const char* host;
    int port;
    int num_headers=0;
    const char* headers[10];
    const char* header_values[10];
    const char* contentType;
    const char* agent;
    int networkDelay;
    int networkTimeout;
};

#endif
