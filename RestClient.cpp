#include "RestClient.h"

RestClient::RestClient(Client* _client, const char* _host) {
  // Serial.print("RestClient->Host:");
  // Serial.println(_host);
  client = _client;
  host = _host;
  port = 80;
  num_headers = 0;
  contentType = "application/json";	// default
  agent = "ArduinoRest/1.1"; //default
  networkDelay = 1000; //default
  networkTimeout = 30*1000; //default
}

RestClient::RestClient(Client* _client, const char* _host, int _port) {
  client = _client;
  host = _host;
  port = _port;
  num_headers = 0;
  contentType = "application/json";	// default
  agent = "ArduinoRest/1.1"; //default
  networkDelay = 1000;
  networkTimeout = 30*1000;
}

void RestClient::setHeader(const char* header, const char* value){
  headers[num_headers] = header;
  header_values[num_headers] = value;
  num_headers++;
}



void RestClient::setContentType(const char* _contentType) {
  contentType = _contentType;
}

void RestClient::setNetworkDelay(int _networkDelay){
  networkDelay = _networkDelay;
}

void RestClient::setNetworkTimeout(int _networkTimeout){
  networkTimeout = _networkTimeout;
}

void RestClient::setAgent(const char* _agent) {
  agent = _agent;
}

void RestClient::setHostPort(const char* _host, int _port){
  host = _host;
  port = _port;
}

void RestClient::write(const char* string){
  DEBUG_REST(string);
  client->print(string);
}

void RestClient::writeln(const char* string){
  DEBUG_REST(string);
  client->println(string);
}

// GET path
int RestClient::get(const char* path){
  return request("GET", path, NULL, NULL);
}

//GET path with response
int RestClient::get(const char* path, String* response){
  return request("GET", path, NULL, response);
}

// POST path and body
int RestClient::post(const char* path, const char* body){
  return request("POST", path, body, NULL);
}

// POST path and body with response
int RestClient::post(const char* path, const char* body, String* response){
  return request("POST", path, body, response);
}

// POST path and body with response
int RestClient::put(const char* path, const char* body, String* response){
  return request("PUT", path, body, response);
}

int RestClient::request(const char* method, const char* path,
                  const char* body, String* response) {

    // close any connection before send a new request.
    // This will free the socket on the WiFi shield
    int statusCode = -1;
	
    client->stop();
    // if there's a successful connection:
	// Serial.println();
  DEBUG_REST("connecting to: ");
  DEBUG_REST(host);
  DEBUG_REST("port:");
  DEBUG_REST(port);
  DEBUG_REST("path:");
  DEBUG_REST(path);
    if (client->connect(host, port)) {
      /*Serial.println("connecting...");
      Serial.print(method);
      Serial.print(" ");
      Serial.print(path);
      Serial.print(" ");
      Serial.println("HTTP/1.1");
      Serial.print("Host: ");
      Serial.println(host);
      Serial.print("User-Agent: ");
      Serial.println(agent);
      Serial.println("Connection: close");*/

      if(body != NULL) {
        DEBUG_REST("Content-Length: ");
        DEBUG_REST(strlen(body));
        DEBUG_REST("Content-type: ");
        DEBUG_REST(contentType);
        DEBUG_REST(body);
      }
      //  post data
      // send the HTTP request:
      write(method);
      write(" ");
      write(path);
      write(" ");
      writeln("HTTP/1.1");
      
      write("Host: ");
      writeln(host);
      write("User-Agent: ");
      writeln(agent);
      write("content-type: ");
      writeln(contentType);
      for(int i=0; i<num_headers; i++){
        write(headers[i]);
        write(": ");
        writeln(header_values[i]);
      }
      writeln("Connection: close");
      if(body != NULL) {
        write("Content-Length: ");
        char contentLength[20];
        sprintf(contentLength, "%d", strlen(body));
        writeln(contentLength);
        writeln("");
        writeln(body);
      }
      writeln("");
      delay (300);
      statusCode = readResponse(response);
      Serial.println(statusCode);
    } else {
      // if you couldn't make a connection:
      Serial.println("connection failed");
    }
    return statusCode;
}

int RestClient::readResponse(String* response) {
  // an http request ends with a blank line
  boolean currentLineIsBlank = true;
  boolean httpBody = false;
  boolean inStatus = false;
  unsigned long timeoutStart = millis();
  char statusCode[4];
  int i = 0;
  int code = 0;
  while ( client->connected() || client->available() &&
          (millis() - timeoutStart) < networkTimeout) {

    if (client->available()) {
      char c = client->read();
      if(c == ' ' && !inStatus){
        inStatus = true;
      }
      if(inStatus && i < 3 && c != ' '){
        statusCode[i] = c;
        i++;
      }
      if(i == 3){
        statusCode[i] = '\0';
        code = atoi(statusCode);
        if (response == NULL) {
          return code;
        }
      }
      if(httpBody){
        //only write response if its not null
        if(response != NULL) response->concat(c);
      }
      else {
          if (c == '\n' && currentLineIsBlank) {
            httpBody = true;
          }
          if (c == '\n') {
            // you're starting a new line
            currentLineIsBlank = true;
          }
          else if (c != '\r') {
            // you've gotten a character on the current line
            currentLineIsBlank = false;
          }
      }
      timeoutStart = millis();
    } else {
      delay(networkDelay);
    }
  }
  return code;
}
