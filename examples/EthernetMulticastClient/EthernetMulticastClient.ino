/*
* arduino code targeted to arduino Ethernet board, developed on Arduino IDE 1.6.7.
*
* This code get the environment temperatura through temperature sensor DS18b20,
*  for that use the <DS1820> library. Once the temperature is obtained it's
*  sended with the timestamp to the server in json format through the <MulticastClient>
*  library.
*
* - If no sensor is detected, no data will be send.
* - The USER_KEY, SENSOR_KEY and SENSOR_KEY_STREAM must be defined and valid on the server.
* - The server's url and port must be defined, or de code will use default values.
*/


#include <SPI.h>
#include <Ethernet.h>
#include <MulticastClient.h>
#include <ArduinoJson.h>
#include <Time.h>

#include <OneWire.h>
#include "DS1820.h"

// =============== timers, pins =====================
#define pinSensor 8
#define numDS 1
#define TIMER_ENVIO 30000 // interval between data submissions

//========================== user token ====================
#define USER_KEY "cba991ae0c38408a84cf01687c60d075"

//=============== Sensors Key , Stream Key ==================


//#define  SENSOR_KEY "9887c50623bd4584bd106d37d84d6bcc"           // Sensor 001
//#define  SENSOR_KEY_STREAM "985bf2cde9b54a54b8fcd3423d89ad89"   // Sensor 001 stream

//#define  SENSOR_KEY "5fd52f447a2f453eaf73723e46759cc3"           // Sensor 002
//#define  SENSOR_KEY_STREAM "438bdfea6d254e3d873292de2da285dd"   // Sensor 002 stream

// #define  SENSOR_KEY "c0b79ec266af4c01acc33a9b84240eb7"           // Sensor 003
// #define  SENSOR_KEY_STREAM "96ad88fd59a044bfa3929e243da383bc"   // Sensor 003 stream

// #define  SENSOR_KEY "878d1cc354cc40e795e99e40e9fe9e8a"           // Sensor 004
// #define  SENSOR_KEY_STREAM "f9e75e128e33449285e7f83b04fab6dd"   // Sensor 004 stream

#define  SENSOR_KEY "25cc1a5f2b1f4ccd9c5d5d5cd39ffb7b"           // Sensor 005
#define  SENSOR_KEY_STREAM "86763252b78f4bc386e4e4f933e5b352"   // Sensor 005 stream


/*
"oid":4,"key":"9887c50623bd4584bd106d37d84d6bcc","name":"wifi001","description":"wifi001"
"streams":[{"oid":6,"key":"985bf2cde9b54a54b8fcd3423d89ad89","name":"temperature"}]

"oid":5,"key":"5fd52f447a2f453eaf73723e46759cc3","name":"wifi002","description":"wifi002",
"streams":[{"oid":7,"key":"438bdfea6d254e3d873292de2da285dd","name":"temperature"}]

"oid":6,"key":"c0b79ec266af4c01acc33a9b84240eb7","name":"eth003","description":"eth003"
"streams":[{"oid":8,"key":"96ad88fd59a044bfa3929e243da383bc","name":"temperature"}]

"oid":7,"key":"878d1cc354cc40e795e99e40e9fe9e8a","name":"eth004","description":"eth004","owner"
"streams":[{"oid":9,"key":"f9e75e128e33449285e7f83b04fab6dd","name":"temperature"}]

"oid":8,"key":"25cc1a5f2b1f4ccd9c5d5d5cd39ffb7b","name":"eth005","description":"eth005"
"streams":[{"oid":10,"key":"86763252b78f4bc386e4e4f933e5b352","name":"temperature"}]
*/

/*================== host ======================*/
static const char* dataHost = "multicast.vix.br";


/* ============Prototipos============== */
unsigned long getTime();
void manageConnection();   // chek if connected, if not try reconnect
void printNetWorkStatus(); // print local ip
void cleanJsonString(char* str, char* result);

// Initialize the Ethernet client library
EthernetClient client;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
unsigned int CHECK_CONNECTION = 0;

//============ Sensor ===================
// initialize parameters of the sensor
// DS1820 <name>(pin_of_sensor, quantity_of_sensor);
DS1820 mySensors(8,1);

void setup() {

 Serial.begin(115200);
 delay(3000);
 // to avoid multiple devices with same mac
 // mac[5] =0x03; // eth003
 // mac[5] =0x04; // eth004
 mac[5] =0x05; // eth005
 manageConnection();
 setTime(getTime());

}

void loop() {
  sendData();
  manageConnection();
}


int sendData()
{
  static unsigned long lastExec = 0;
  if( (millis() - lastExec) > TIMER_ENVIO){
  lastExec  = millis();
  
  MulticastClient multicast  = MulticastClient(&client, SENSOR_KEY,
     SENSOR_KEY_STREAM,USER_KEY);
  /*
  * Set the host and port that will receive data. If not seted the libray will
  * use default host defined inside MulticastClient.h file and port 80.
  */
   multicast.MultCastSetHost_Port(dataHost, 8082);
  /*
  *  for 1 sensor on bus.
  *  getTemperature() return the integer value of the temperature * 10.
  *  ex:    room temperature 22,5C will be 225;
  *
  */
  float value = mySensors.getTemperature()/10.0;
  /*
  * if the address is 0 the was a fail in the reading process or the sensor * was not found, so don't send data to the server.
  */
    if(mySensors.getAddressSensor() != 0) {
      int statusCode = multicast.postData(now(), value);
      return statusCode;
    }
  }
}
// Get the timestamp from server
unsigned long getTime() {
  RestClient rest = RestClient(&client, "api.timezonedb.com");
  String response;
  int statusCode = rest.get("/?zone=America/Sao_Paulo&key=0XB1Z06VAUM2&format=json", &response);
  char* buffer = (char*) malloc(response.length()*sizeof(char));
  response.toCharArray(buffer, response.length());
  char* result = (char*) malloc(156*sizeof(char));
  result[0] = '\0';
  cleanJsonString(buffer, result);
  free(buffer);
  const int JSON_BUFFER = JSON_OBJECT_SIZE(8);
  StaticJsonBuffer<JSON_BUFFER> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(result);
  free(result);
  unsigned long temp = root[F("timestamp")];
  Serial.println(temp);
  return temp;
}

// Clean the server response leaving only the json string
void cleanJsonString(char* str, char* result) {
  int len = strlen(str) - 1;
  int start, finish, i;
  for (i = 0; str[i] != '{'; i++ );
  start = i;
  for (i = len; str[i] != '}'; i-- );
  finish = i;
  int j = 0;
  for (i = start; i <= finish; i++) {
    result[j] = str[i];
    j++;
  }
  result[j] = '\0';
}
